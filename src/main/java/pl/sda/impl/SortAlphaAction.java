package pl.sda.impl;

import pl.sda.Action;

import java.util.List;
import java.util.stream.Collectors;

public class SortAlphaAction implements Action {
    @Override
    public String doIt(List<String> strings) {
        return strings.stream()
                .sorted()
                .collect(Collectors.joining(" "));
    }
}
