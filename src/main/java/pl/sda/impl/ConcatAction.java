package pl.sda.impl;

import pl.sda.Action;

import java.util.List;

public class ConcatAction implements Action {
    public String doIt(List<String> strings) {
        return String.join("", strings);
    }
}
